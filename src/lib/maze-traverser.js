const _ = require('lodash');
const Immutable = require('immutable');

/*
 * cursor: immutable js map
 *  x(number), y(number), direction:('N', 'S', 'E', 'W')
 */

const changePositionTable = {
  'N': (cursor) => { return cursor.set('y', cursor.get('y') + 1) },
  'S': (cursor) => { return cursor.set('y', cursor.get('y') - 1) },
  'E': (cursor) => { return cursor.set('x', cursor.get('x') + 1) },
  'W': (cursor) => { return cursor.set('x', cursor.get('x') - 1) }
};

const changeDirectionTable = {
  '/': (cursor) => {
    const changeTable = {
      'N':'E',
      'S':'W',
      'E':'N',
      'W':'S'
    };
    return cursor.set('direction', changeTable[cursor.get('direction')]);
  },
  '\\': (cursor) => {
    const changeTable = {
      'N':'W',
      'S':'E',
      'E':'S',
      'W':'N'
    };
    return cursor.set('direction', changeTable[cursor.get('direction')]);
  }
};

//moves the cursor forward in whatever direction its suppossed to go
function progressCursor(cursor, mirror) {
  if (mirror) {
    const newCursor = changeDirectionTable[mirror](cursor);
    return changePositionTable[newCursor.get('direction')](newCursor);
  } else {
    return changePositionTable[cursor.get('direction')](cursor);
  }
}

function outOfBounds(dimensions, cursor) {
  if (cursor.get('x') >= dimensions.get('x')) {
    return true;
  } else if (cursor.get('x') < 0) {
    return true;
  } else if (cursor.get('y') >= dimensions.get('y')) {
    return true;
  } else if (cursor.get('y') < 0) {
    return true;
  } else {
    return false;
  }
}

//boolean, returns true if at an edge
function approachingEdge(dimensions, cursor, mirror) {
  //cursor edge returns true if new Cursor is out of bounds
  return outOfBounds(dimensions, progressCursor(cursor, mirror));
}

function getBreadcrumb(cursor) {
  const  { x, y, direction } = cursor.toJS();
  return [ x, y, direction ].join(',');
}

function getCoordinateString(cursor) {
  return ['x', 'y'].map((attr) => { return cursor.get(attr)}).join(',')
}

function tailSolve(maze, pathCache, path, cursor) {
  //console.log(JSON.stringify(path.toJS(), null, 2));
  //if its one by one
  const dimensions = maze.get('dimensions');
  /*
   * base case, if an existing direction + coordinates exists in the pathCache, 
   * then we are in a loop and by default, resturn -1
   */
  if (pathCache.get(getBreadcrumb(cursor))) {
    return Immutable.Map({
      squares: -1
    });
  }

  //if the cursor is at an edge, return the path and the final coordinates
  const atEdge = approachingEdge(dimensions,
                      cursor,
                      maze
                        .get('mirrors')
                        .get(getCoordinateString(cursor)));

  if (atEdge) {
    return Immutable.Map({
      squares: path.size,
      path: path.shift().push(cursor),
      coordinates: Immutable.Map({
        x: cursor.get('x'),
        y: cursor.get('y')
      })
    });
  }

  return tailSolve(maze,
                   pathCache.set(getBreadcrumb(cursor), true),
                   path.push(cursor),
                   progressCursor(cursor, maze.get('mirrors').get(getCoordinateString(cursor))));
}

//assumes the maze is an immutablejs map
function solve(maze) {
  const cursor = maze.get('player');

  const dimensions = maze.get('dimensions');

  if ((dimensions.get('x') === 1 ) && dimensions.get('y') === 1) {
    return Immutable.fromJS({
      squares: 0,
      path: [],
      coordinates: {
        x: 0,
        y: 0
      }
    })
  }
  return tailSolve(maze,
                   Immutable.Map().set(),
                   Immutable.List(),
                   cursor);
}

module.exports.solve = solve;



