
const { loadFile } = require('./lib/file-loader');
const { solve }    = require('./lib/maze-traverser');



function solveMaze(file) {
  return loadFile(file)
  .then(solve);
}


module.exports.solveMaze = solveMaze;


