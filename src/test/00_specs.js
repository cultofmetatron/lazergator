const mocha = require('mocha');
const should = require('should');
const path = require('path');
const sampleDataPath = path.join(__dirname, '..', '..', 'samples-data');

describe('file-loader', function() {
  const { loadFile } = require('../lib/file-loader');
  it('should load a file', function(done) {
    loadFile(path.join(sampleDataPath, 'basic.dat'))
    .then(function(data) {
        done()
    })
    .catch(done);
  })
});

describe('maze-traverser', function() {
  const { loadFile } = require('../lib/file-loader');
  const { solve }    = require('../lib/maze-traverser');
  it('should solve the maze', function(done) {
    loadFile(path.join(sampleDataPath, 'basic.dat'))
    .then((maze) => {
      return solve(maze)
    })
    .then((solution) => {
      solution.get('squares').should.equal(9)
      let coordinates = solution.get('coordinates')
      coordinates.get('x').should.equal(0);
      coordinates.get('y').should.equal(0);
      done();
    })
    .catch(done);
  });

  it('should solve 1 square case', function() {
    loadFile(path.join(sampleDataPath, 'single.dat'))
    .then(solve)
    .then((solution) => {
      let { squares, coordinates } = solution.toJS();
      squares.should.equal(0);
      coordinates.x.should.equal(0);
      coordinates.y.should.equal(0);
    });
  });

  it('should solve the big kahuna', function(done) {
    this.timeout(50000);
    loadFile(path.join(sampleDataPath, 'bigkahuna.dat'))
    .then(solve)
    .then((solution) => {
      let { squares, coordinates } = solution.toJS();
      //squares.should.equal(0);
      coordinates.x.should.equal(999);
      coordinates.y.should.equal(500);
      done();
    })
    .catch(done)

  });


  it('should return -1 if the maze is unsolvable', function(done) {
    loadFile(path.join(sampleDataPath, 'cycle.dat'))
    .then(solve)
    .then((solution) => {
      solution.get('squares').should.equal(-1)
      done();
    })
    .catch(done);

  });




})




