var gulp = require('gulp');
var babel = require('gulp-babel');
var path = require('path');
var mocha = require('gulp-mocha');

gulp.task('build', function() {
    return gulp.src(path.join(__dirname, 'src/**/*.js'))
        .pipe(babel())
        .pipe(gulp.dest('dist'));
});

gulp.task('test', function() {
   return gulp.src(path.join(__dirname, 'dist/test/**/*.js'))
    .pipe(mocha({}));
})
