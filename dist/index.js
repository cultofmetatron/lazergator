'use strict';

var _require = require('./lib/file-loader');

var loadFile = _require.loadFile;

var _require2 = require('./lib/maze-traverser');

var solve = _require2.solve;

function solveMaze(file) {
  return loadFile(file).then(solve);
}

module.exports.solveMaze = solveMaze;