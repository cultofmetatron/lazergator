'use strict';

var _require = require('bluebird');

var promisifyAll = _require.promisifyAll;

var fs = promisifyAll(require('fs'));
var Immutable = require('immutable');
var _ = require('lodash');

function createBase(sets) {
  var size = sets[0];
  var player = sets[1];
  return {
    dimensions: {
      x: size[0],
      y: size[1]
    },
    player: {
      x: player[0],
      y: player[1],
      direction: player[2]
    }
  };
}

function createMaze(sets) {
  //this is the base of the structure
  var base = createBase(sets.slice(0, 2));
  var mirrors = _.reduce(sets.slice(2), function (memo, mirror) {
    memo[mirror.slice(0, 2).join(',')] = mirror[2];
    return memo;
  }, {});
  //add the mirrors and we good
  return _.extend({}, base, {
    mirrors: mirrors
  });
};

//takes contents and returns an array of relevant data for each line
function extractSets(contents) {
  return _(contents.split('\n')).chain().map(function (chunk, index) {
    return chunk.split(' ');
  }).filter(function (chunk) {
    return chunk.join() !== "";
  }) //filters out empty lines
  .map(function (packet) {
    var coordinates = packet.slice(0, 2).map(function (num) {
      var value = parseInt(num);
      if (NaN) {
        throw new Error('NaNException');
      }
      return value;
    });
    //using concat to avoid mutation
    return _.isUndefined(packet[2]) ? coordinates : coordinates.concat([packet[2]]);
  }).value();
}

function pipeline(srcPromise) {
  return srcPromise.then(extractSets).then(createMaze).then(function (data) {
    return Immutable.fromJS(data);
  });
}

//in case I need to load the source itself
function loadSrc(src) {
  return pipeline(new Promise(function (resolve, reject) {
    resolve(src);
  }));
}

function loadFile(file) {
  return pipeline(fs.readFileAsync(file, 'utf8'));
};

module.exports.extractSets = extractSets;
module.exports.createBase = createBase;
module.exports.createMaze = createMaze;
module.exports.loadFile = loadFile;
module.exports.loadSrc = loadSrc;