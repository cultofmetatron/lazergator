'use strict';

var mocha = require('mocha');
var should = require('should');
var path = require('path');
var sampleDataPath = path.join(__dirname, '..', '..', 'samples-data');

describe('file-loader', function () {
  var _require = require('../lib/file-loader');

  var loadFile = _require.loadFile;

  it('should load a file', function (done) {
    loadFile(path.join(sampleDataPath, 'basic.dat')).then(function (data) {
      done();
    })['catch'](done);
  });
});

describe('maze-traverser', function () {
  var _require2 = require('../lib/file-loader');

  var loadFile = _require2.loadFile;

  var _require3 = require('../lib/maze-traverser');

  var solve = _require3.solve;

  it('should solve the maze', function (done) {
    loadFile(path.join(sampleDataPath, 'basic.dat')).then(function (maze) {
      return solve(maze);
    }).then(function (solution) {
      solution.get('squares').should.equal(9);
      var coordinates = solution.get('coordinates');
      coordinates.get('x').should.equal(0);
      coordinates.get('y').should.equal(0);
      done();
    })['catch'](done);
  });

  it('should solve 1 square case', function () {
    loadFile(path.join(sampleDataPath, 'single.dat')).then(solve).then(function (solution) {
      var _solution$toJS = solution.toJS();

      var squares = _solution$toJS.squares;
      var coordinates = _solution$toJS.coordinates;

      squares.should.equal(0);
      coordinates.x.should.equal(0);
      coordinates.y.should.equal(0);
    });
  });

  it('should solve the big kahuna', function (done) {
    this.timeout(50000);
    loadFile(path.join(sampleDataPath, 'bigkahuna.dat')).then(solve).then(function (solution) {
      var _solution$toJS2 = solution.toJS();

      //squares.should.equal(0);
      var squares = _solution$toJS2.squares;
      var coordinates = _solution$toJS2.coordinates;
      coordinates.x.should.equal(999);
      coordinates.y.should.equal(500);
      done();
    })['catch'](done);
  });

  it('should return -1 if the maze is unsolvable', function (done) {
    loadFile(path.join(sampleDataPath, 'cycle.dat')).then(solve).then(function (solution) {
      solution.get('squares').should.equal(-1);
      done();
    })['catch'](done);
  });
});